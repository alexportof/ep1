#ifndef BALLOT_BOX_HPP
#define BALLOT_BOX_HPP

#include <string>
#include "voter.hpp"
#include "candidate.hpp"
using namespace std;

class Ballot_box
{
	private:	
	public:
		Ballot_box();
		~Ballot_box();
		
		string catch_district_deputy_vote(Voter voter);
		string catch_congressman_vote(Voter voter);
		string catch_senator1_vote(Voter voter);
		string catch_senator2_vote(Voter voter);
		string catch_governor_vote(Voter voter);
		string catch_president_vote(Voter voter);
		void start_voting(Voter *voter,Candidate DFcandidate[],Candidate BRcandidate[]);
		int senator_vote;
		void show_candidate(Candidate candidate);
		void counting_of_votes(Candidate DFcandidate[],Candidate BRcandidate[]);
};

#endif