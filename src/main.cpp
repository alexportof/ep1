#include "candidate.hpp"
#include "voter.hpp"
#include "ballot_box.hpp"
#include <iostream>


using namespace std;




int main(int argc, char ** argv){

	int number_of_voters;
	Ballot_box *ballot_box;
	ballot_box = new Ballot_box();	
	cout << "Qual a quantidade de eleitores que votarão nessa urna?" << endl;
	cin >> number_of_voters;	
	Voter *voter[1000];

	ifstream ip("./data/consulta_cand_2018_BR.csv");
	if(!ip.is_open()) cout << "Error" << endl;

	string name;
	string role;
	string state;
	string political_name;
	string code;
	int i=0;
	Candidate BRcandidates[13];

	while(ip.good()){
		BRcandidates[i] = Candidate();
		getline(ip,state,',');
		BRcandidates[i].set_state(state);
		getline(ip,role,',');
		BRcandidates[i].set_role(role);
		getline(ip,code,',');
		BRcandidates[i].code = code;
		getline(ip,name,',');
		BRcandidates[i].name = name;
		getline(ip,political_name,'\n');
		BRcandidates[i].set_political_name(political_name);
		i++;
	}
	ip.close();

	ifstream ip2("./data/consulta_cand_2018_DF.csv");
	if(!ip2.is_open()) cout << "Error" << endl;
	 i=0;
	Candidate DFcandidates[1183];

	while(ip2.good()){
		DFcandidates[i] = Candidate();
		getline(ip2,state,',');
		DFcandidates[i].set_state(state);
		getline(ip2,role,',');
		DFcandidates[i].set_role(role);
		getline(ip2,code,',');
		DFcandidates[i].code = code;
		getline(ip2,name,',');
		DFcandidates[i].name = name;
		getline(ip2,political_name,'\n');
		DFcandidates[i].set_political_name(political_name);
		i++;
	}
		ip2.close();


 	

	for(int i=0;i<number_of_voters;i++){
		voter[i] = new Voter();
		ballot_box->start_voting(voter[i],DFcandidates,BRcandidates);

	}
	for(int i=0;i<number_of_voters;i++){
		cout << endl <<voter[i]->get_voter_name() <<" votou em: " << endl;
		cout << "Presidente: "<<voter[i]->get_president_name() << endl;
		cout << "Governador: "<<voter[i]->get_governor_name() << endl;
		cout << "Senador 1: "<<voter[i]->get_senator1_name() << endl;
		cout << "Senador 2: "<<voter[i]->get_senator2_name() << endl;
		cout << "Deputado Federal: "<<voter[i]->get_congressman_name() << endl;
		cout << "Deputado Distrital: "<<voter[i]->get_district_deputy_name()<< endl;
	}
	ballot_box->counting_of_votes(DFcandidates,BRcandidates);
	
	return 0;
}