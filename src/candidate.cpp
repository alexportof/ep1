#include "candidate.hpp"
#include <iostream>
#include <fstream>

using namespace std;

Candidate::Candidate(){
	quantity_of_votes = 0;
}
Candidate::Candidate(string role,string state,string political_name){
	set_role(role);
	set_state(state);
	set_political_name(political_name);
}

Candidate::~Candidate(){}

string Candidate::get_role(){
	return role;
}
void Candidate::set_role(string role){
	this->role = role;
}
string Candidate::get_state(){
	return state;
}
void Candidate::set_state(string state){
	this-> state = state;
}
string Candidate::get_political_name(){
	return political_name;
}
void Candidate::set_political_name(string political_name){
	this-> political_name = political_name;
}